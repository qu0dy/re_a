# -*- coding: utf-8 -*-
from scrapy import Spider, Request
from spiders.items import SergiorossiItem
import datetime
try:
    from urllib.parse import urljoin
except:
    from urlparse import urljoin

class SergiorossiSpider(Spider):
    name = "sergiorossi"
    allowed_domains = ["sergiorossi.com"]
    countries = ['al', 'dz', 'ad', 'ar', 'am', 'au', 'at', 'az', 'bh', 'by',
                 'be', 'ba', 'bn', 'bg', 'ca', 'cl', 'cn', 'co', 'ci', 'hr',
                 'cy', 'cz', 'dk', 'do', 'eg', 'ee', 'fi', 'fr', 'ge', 'de',
                 'gr', 'gt', 'va', 'hk', 'hu', 'is', 'in', 'id', 'ie', 'il',
                 'it', 'jp', 'jo', 'kz', 'kw', 'kg', 'lv', 'lb', 'lr', 'li',
                 'lt', 'lu', 'mo', 'mk', 'mg', 'my', 'mt', 'mx', 'md', 'mc',
                 'me', 'ma', 'nl', 'nz', 'no', 'om', 'pa', 'py', 'pe', 'ph',
                 'pl', 'pt', 'qa', 'ro', 'ru', 'sm', 'sa', 'rs', 'sg', 'sk',
                 'si', 'za', 'kr', 'es', 'sr', 'se', 'ch', 'sy', 'tw', 'tj',
                 'th', 'tn', 'tr', 'tm', 'ua', 'ae', 'gb', 'us', 'uz', 've',
                 'vn']

    start_url = 'http://www.sergiorossi.com/{country}'

    def start_requests(self):
        for country in self.countries:
            yield Request(self.start_url.format(country=country),
                          callback=self.parse,
                          dont_filter=True,
                          cookies={}
                          )

    def parse(self, response):
        try:
            urls = response.xpath(
                '//*[@id="siteNav"]//li[contains(@class, "list1")]/a/@href'
            ).extract()[:3]
        except:
            urls = []
        for url_index, url in enumerate(urls):
            if url_index == 0 :
                u = []
                u.append("/".join(urls[1].split('/')[:-1]) + '/prefall-2016-apac_gid33622')
                u.append("/".join(urls[1].split('/')[:-1]) + '/prefall-2016-emea_gid33621')
                u.append("/".join(urls[1].split('/')[:-1]) + '/prefall-2016-emea_gid33620')
                u = list(map(lambda x: urljoin(response.url, x), u))
                for _u in u:
                    yield Request(_u,
                                  callback=self.parse_page,
                                  dont_filter=True)
            else:
                url = urljoin(response.url, url)
                yield Request(url,
                              callback=self.parse_page,
                              dont_filter=True)

    def parse_page(self, response):
        products = response.xpath('//div[@class="itemContainer"]')
        country = response.xpath(
            '//ul[contains(@class, "footerListBottomLeft")]//text()').re('\: (.+?) \(')
        try:
            currency = response.xpath(
                '//script[contains(text(), "currency")]').re('currency\: \'(.+?)\'')[0]
        except:
            currency = ''
        for product_selector in products:
            item = SergiorossiItem()
            item['productcode'] = product_selector.xpath('@data-cod10').extract()
            item['gender'] = 'W'
            try:
                fullprice = product_selector.xpath(
                    '*//span[@class="itemPrice sconto"]/text()').extract()[0].replace(currency, '').strip()
                item['fullprice'] = fullprice
            except IndexError:
                fullprice = 0
            try:
                price = product_selector.xpath(
                    '*//span[@class="itemPrice"]/text()').extract()[0].replace(currency, '').strip()
                item['price'] = price
            except IndexError:
                price = 0
            item['currency'] = currency
            item['country'] = country
            try:
                item['itemurl'] = urljoin(response.url, product_selector.xpath(
                    '*//a/@href').extract()[0])
            except IndexError:
                pass
            item['brand'] = 'SERGIOROSSI'
            item['website'] = 'SERGIOROSSI'
            item['competence_date'] = datetime.datetime.today().strftime('%Y%m%d')
            item['pricemax'] = 0
            yield item